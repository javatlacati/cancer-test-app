# README / LÉAME #

Cancer Test is an Application designed with the purpose of diagnose cancer so it should be considered as a mental effort, but never a final professional diagnostic. We create it with the aim of get massive access to the common people and hopefully save some lifes.

### Index / Índice ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? / ¿cómo lo configuro? ###

Para configurarlo se recomienda usar netbeans.

* Configuration / configuración

    1. Instalar nodejs
    2. instalar cordova
    3. Instalar el android sdk
    4. instalar Java ( JDK + JRE )
    5. Instalar Netbeans ( Recomendado )

En el Netbeans ir a: Team > Git > Clone

En el primer campo escriba el url de este repositorio, en los siguientes su usuario y contraseña.

![Sin título.png](https://bitbucket.org/repo/Xbjn7b/images/2943126240-Sin%20t%C3%ADtulo.png)

Seleccione el brazo a copiar, si no sabe cual elegir elija el brazo master
![Sin título.png](https://bitbucket.org/repo/Xbjn7b/images/1458960406-Sin%20t%C3%ADtulo.png)

Finalmente configure en que lugar se copiará en su pc. Si no sabe como configurarlo le sugerimos llarno de manera similar a la siguiente pantalla.
![Sin título.png](https://bitbucket.org/repo/Xbjn7b/images/2524443392-Sin%20t%C3%ADtulo.png)

* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines / ¿Cómo contribuyo? ###

## Info / Información ##
 
 Lo más importante es recabar informaciíon, si cuenta con ella diríjase al archivo fichas.md

![fichas.png](https://bitbucket.org/repo/Xbjn7b/images/1261494152-Sin%20t%C3%ADtulo.png)

Dé click en editar, y cuando termine en comit. Recuerde que la sintaxis que se usa es en el lenguaje markdown. Si no sabe ese lenguaje, limítese a usar texto.

## Obtener el estado actual del repositorio ##

Para obtener el estado actual en que se encuentra el proyecto en nuestro netbeans:

1.  seleccionamos nuestro proyecto y damos click en Git > Remote > Fetch A este proceso se le conoce como ´Fetch´

![Fetch.png](https://bitbucket.org/repo/Xbjn7b/images/1845929987-Sin%20t%C3%ADtulo.png)

2. Si no hemos configurado nuestra cuenta ingresamos los datos de acceso, si no ya estarán guardados. Damos click en Next

![Credentials.png](https://bitbucket.org/repo/Xbjn7b/images/3856820651-Sin%20t%C3%ADtulo.png)


3. Seleccionamos el brazo al que queremos actualizar

![Sin título.png](https://bitbucket.org/repo/Xbjn7b/images/1458960406-Sin%20t%C3%ADtulo.png)

4. Seleccionamos el brazo que tengamos en nuestra pc en el que queramos que se sincronice la información ( Se recomienda tener máximo dos a la vez )

![Brazolocal.png](https://bitbucket.org/repo/Xbjn7b/images/1918693104-Sin%20t%C3%ADtulo.png)
# README / LÉAME #

Cancer Test is an Application designed with the purpose of diagnose cancer so it should be considered as a mental effort, but never a final professional diagnostic. We create it with the aim of get massive access to the common people and hopefully save some lifes.

### Index / Índice ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? / ¿cómo lo configuro? ###

Para configurarlo se recomienda usar netbeans.

* Configuration / configuración

    1. Instalar nodejs
    2. instalar cordova
    3. Instalar el android sdk
    4. instalar Java ( JDK + JRE )
    5. Instalar Netbeans ( Recomendado )

En el Netbeans ir a: Team > Git > Clone

En el primer campo escriba el url de este repositorio, en los siguientes su usuario y contraseña.

![Sin título.png](https://bitbucket.org/repo/Xbjn7b/images/2943126240-Sin%20t%C3%ADtulo.png)

Seleccione el brazo a copiar, si no sabe cual elegir elija el brazo master
![Sin título.png](https://bitbucket.org/repo/Xbjn7b/images/1458960406-Sin%20t%C3%ADtulo.png)

Finalmente configure en que lugar se copiará en su pc. Si no sabe como configurarlo le sugerimos llarno de manera similar a la siguiente pantalla.
![Sin título.png](https://bitbucket.org/repo/Xbjn7b/images/2524443392-Sin%20t%C3%ADtulo.png)

* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines / ¿Cómo contribuyo? ###

## Info / Información ##
 
 Lo más importante es recabar informaciíon, si cuenta con ella diríjase al archivo fichas.md

![fichas.png](https://bitbucket.org/repo/Xbjn7b/images/1261494152-Sin%20t%C3%ADtulo.png)

Dé click en editar, y cuando termine en comit. Recuerde que la sintaxis que se usa es en el lenguaje markdown. Si no sabe ese lenguaje, limítese a usar texto.

## Obtener el estado actual del repositorio ##

Para obtener el estado actual en que se encuentra el proyecto en nuestro netbeans:

1.  seleccionamos nuestro proyecto y damos click en Git > Remote > Fetch A este proceso se le conoce como ´Fetch´

![Fetch.png](https://bitbucket.org/repo/Xbjn7b/images/1845929987-Sin%20t%C3%ADtulo.png)

2. Si no hemos configurado nuestra cuenta ingresamos los datos de acceso, si no ya estarán guardados. Damos click en Next

![Credentials.png](https://bitbucket.org/repo/Xbjn7b/images/3856820651-Sin%20t%C3%ADtulo.png)


3. Seleccionamos el brazo al que queremos actualizar

![Sin título.png](https://bitbucket.org/repo/Xbjn7b/images/1458960406-Sin%20t%C3%ADtulo.png)

4. Seleccionamos el brazo que tengamos en nuestra pc en el que queramos que se sincronice la información ( Se recomienda tener máximo dos a la vez )

![Brazolocal.png](https://bitbucket.org/repo/Xbjn7b/images/1918693104-Sin%20t%C3%ADtulo.png)

5. Como ya comparamos los archivos que hay localmente con los que hay en el servidor ahora actualizamos lo que hay en el servidor a nuestra PC haciendo click en Git > Remote > Pull. A este proceso se le conoce como ´pull´

![jalar.png](https://bitbucket.org/repo/Xbjn7b/images/12126544-Sin%20t%C3%ADtulo.png)

Tras finalizar este proceso usted tendrá una copia con lo más actual.
**¡¡Felicidades!!**

## ¿Cómo subo los cambios que he hecho al sistema? ##

1. Como primer paso y para evitar problemas de conflictos con el trabajo de otras personas le recomendamos primero realizar los pasos del apartado Obtener el estado actual del repositorio.

Haga los cambios que considere pertienentes hasta que conformen una unidad lógica, es decir, hasta que peuda referirse a ellos por un nombre descriptivo. Suponga el caso del commit 67e89a0. Donde faltaba una llave de cierre.

2. Seleccione la opción Git > Commit y una vez allí, expliue de que se trata su cambio y seleccione los archivos que componen el cambio. Tras finalizar este paso, se habrá regiustrado el cambio en un índice de su computadora.

3. Antes de subir debe realizar un fetch y un pull para asegurarse que sus cambios no distorsionan los cambios de otra persona.

Ejemplo de conflicto:

Suponga que tiene un archivo que diga:

<h1> Hola mundo <h>

<ul>cruel</ul>

Usted lo edita y lo deja como:

<h1> Hola mundo </h1>

<ul>cruel </ul>

pero al subirlo se da cuenta que alguien ya propuso un cambio, y el archivo actual dice

<b> Hola mundo</b>

<ul>cruel</ul>


4. Para compartir todos lso cambios que ha registrado desde la última vez seleccione Git > Remote > Push, a esto se le conoce como push y significa básicamente que inserttará uno o más cambios para que todos los vean.

![push.png](https://bitbucket.org/repo/Xbjn7b/images/14236630-Sin%20t%C3%ADtulo.png)


* Writing tests / escribiendo pruebas

El sistema usa un árbol de inferencias que se simplifica mediante el algoritmo id3 ( Induction Desition Tree ) que es un algoritmo común en datamininng.
Para que se obtenga un árbol sin casos que no se puedan diagnosticar se deben de ver todas las combinaciones posibles para cada síntoma o característica a considerar.

* Code review / Revisión de código


* Other guidelines / Otros lineamientos


## soy muy pro y quiero usar la consola ##

### Cómo hago un fetch? ###

git remote -v
setting up remote: origin
git fetch https://javatlacati@bitbucket.org/javatlacati/cancer-test-app +refs/heads/master:refs/remotes/origin/master

( Si hay cambios hacer un merge )

git merge origin/master

### Cómo hago un commit? ###

git add C:\Users\Administrador\Documents\NetBeansProjects\CancerTestApp\README.md
git commit -m Prueba de edición C:\Users\Administrador\Documents\NetBeansProjects\CancerTestApp\README.md


### Cómo hago un push? ###

git branch
git remote -v
setting up remote: origin
git submodule status
git push https://javatlacati@bitbucket.org/javatlacati/cancer-test-app refs/heads/master:refs/heads/master


### Who do I talk to? / ¿Cómo los contacto? ###

* Repo owner / dueño:

    * Ruslan López Carro <[javatlacati](mailto:scherzo16@gmail.com)>