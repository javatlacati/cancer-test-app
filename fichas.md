** Este archivo es para ir poniendo la información que encontremos acerca del diagnóstico de cáncer **

Podemos poner citas, o algunos casos no contemplados. No olviden agregar la `Fuente Bibliográfica` o `Fuente Hemerográfica` de la que sacaron al info.

Los cinco cánceres 'mayores' que causan al mayor mortalidad a nivel mundial son:

1. Pulmón: 1.3 millones
2. Estómago: 1 millón
3. Hígado: 662 mil
4. Colon: 655 mil
5. Pecho: 502 mil

Los cinco tipos de cánceres más counes son:

1. Pulmón
2. Estómago
3. Hígado
4. Colorectal
5. Esófago

1/5 de los cánceres son causados por alguna infección crónica.

Methods of Cancer Diagnosis, Therapy, and Prognosis
Volume 1 Springer 2008

Nódulos o masas palpables en el pecho sin algún otro síntoma clínico caracteriza a carcinoma sebaceo de pecho.
Methods of Cancer Diagnosis, Therapy, and Prognosis
Volume 1 Springer 2008


***Síntomas generales.***

* Pérdida de peso inexplicable de 10 o más.
* La fiebre puede ser un signo temprano de cáncer de leucemia o el linfoma.
* Cansancio extremo que no mejora con el descanso.
* Dolor síntoma de canceres como el de huesos o el testicular.
* Dolor de cabeza que no desaparece puede ser síntoma de un tumor cerebral.
* Dolor de espalda puede ser síntoma de cáncer de colon, recto u ovario.
* Cambios en la piel como oscurecimiento, coloración amarillenta, enrojecimiento, picazón.

***Cambio en el hábito de evacuación.***

*	Estreñimiento, la diarrea o un cambio en el tamaño de las heces fecales puede que sea un signo de cáncer de colon.

***Cambio en la función de la vejiga.***

*	El dolor al orinar sangre en la orina u orinar con mayor o menor frecuencia puede que sean síntomas de canceres de vejiga o próstata.

***Manchas blancas en la lengua o en la boca.***

*	Pudieran ser leucoplaquia, es un área precancerosa que es causada por una irritación frecuente, a menudo es causada por el hábito de fumar.

***Llagas que no cicatrizan.***

*	Una llaga en la boca que no se cura pudiera deberse a un cáncer oral. 

***Sangrado o secreción inusual.***

*	Un sangrado inusual puede ocurrir en caso de cáncer en sus etapas iniciales o avanzadas.
*	Si aparece sangre en el excremento (lo que puede tener un color muy oscuro) podría ser un signo de cáncer de colon o de cáncer de recto.
*	El cáncer de cuello uterino o del endometrio (revestimiento del útero) puede causar sangrado vaginal anormal.
*	La sangre en la orina puede que sea un signo de cáncer de la vejiga o del riñón.
*	Una secreción con sangre que salga del pezón puede que sea un signo de cáncer de seno.

***Endurecimiento o una masa en el seno o en cualquier parte del cuerpo.***

*	Estos canceres se presentan principalmente en los senos, los testículos, los ganglios linfáticos y en los tejidos blandos del cuerpo.

***Indigestión o dificultad para tragar.***

*	La indigestión o dificultan para tragar persistente pueden ser signos de cáncer de esófago, de estómago o de faringe.

***Tos persistente o ronquera.***

*	Una tos persistente puede que sea un signo de cáncer de pulmón, mientras que la ronquera puede ser un signo de cáncer de laringe o de la glándula tiroides. 


cancer.org


*** Síntomas Cáncer del Hígado ***

Los síntomas del cáncer de hígado en las etapas iniciales son vagos y, con frecuencia, pasan desapercibidos.
El cáncer de hígado puede provocar los siguientes síntomas:

* Pérdida de apetito
* Pérdida de peso sin explicación
* Fiebre
* Fatiga
* Debilidad
* Dolor abdominal
* Hinchazón abdominal
* Náuseas
* Orina oscura
* Color amarillento de la piel y/o en el blanco de los ojos

Nota: Estos síntomas también pueden surgir a raíz de otras condiciones de salud menos graves. La persona que sienta dichos síntomas debe consultar a un médico.


***Cáncer del Seno.***

Síntomas:
Al desarrollarse inicialmente el cáncer del seno, puede no presentar ningún síntoma. Sin embargo, a medida que crece el cáncer, puede causar los siguientes cambios:

* Un bulto o espesamiento en el seno o cerca del mismo o en el área de la axila
* Cambio en el tamaño o la forma del seno
* Secreción del pezón o dolor en el mismo, o pezón invertido hacia dentro del pecho
* Arrugas u hoyuelos en la piel del seno (como la cáscara de una naranja)
* Cambio en el aspecto o al palpar la piel del seno, de la areola o del pezón (por ejemplo, caliente, hinchada, roja o escamosa)

Nota: Estos síntomas también pueden ser ocasionados por otros problemas de salud menos graves. Cualquiera que experimente estos síntomas debe consultar un médico


***Cáncer Uterino.***

Los síntomas incluyen:

* Sangramiento anormal entre períodos menstruales
* Sangramiento vaginal o manchas de sangre en las mujeres posmenopáusicas
* Dolor en el área pélvica
* Dolor al orinar
* Dolor durante las relaciones sexuales

Nota: Estos síntomas también pueden surgir a raíz de otras condiciones de salud menos graves. La persona que sienta dichos síntomas debe consultar a un médico.


***Cáncer del Pulmón.***

Los síntomas incluyen:

* Tos que no desaparece o empeora con el tiempo
* Dolor constante en el pecho
* Escupir sangre al toser
* Falta de aire, silbido en el pecho o afonía
* Problemas repetitivos con neumonía o bronquitis
* Hinchazón de cuello y rostro
* Pérdida de apetito o de peso
* Fatiga

Nota: Estos síntomas también pueden surgir a raíz de otras condiciones de salud menos graves. Cualquiera que experimente estos síntomas debe consultar a un médico.


***Cáncer de la Piel.***

Síntomas:
Raramente causan dolor los cánceres de piel. El síntoma inicial más común es algún cambio en la piel, como la aparición de un bulto o una lastimadura que no cicatriza. En su fase inicial, el cáncer de piel puede tener los siguientes aspectos:

* Un bulto pequeño, liso, con brillo, pálido o ceroso
* Un bulto firme y rojo
* Un bulto que sangra o crea una costra
* Una mancha chata y roja que es áspera, seca o escamosa

Se encuentran los cánceres de piel principalmente en áreas de la piel expuestas al sol: la cabeza, orejas, cuello, manos y brazos. Sin embargo, el cáncer de piel puede ocurrir en cualquier lugar.

Nota: Estos síntomas también pueden ser ocasionados por otras condiciones de salud menos graves. La persona que tenga dichos síntomas debe consultar a un médico si duran más de 2 semanas.


***Cáncer del Riñón.***

Los síntomas pueden incluir:

* Sangre en la orina
* Dolor sin explicación en la baja espalda
* Bulto en la panza
* Presión sanguínea elevada
* Pérdida de peso significativa, no planeada
* Fiebre sin explicación
* Hinchazón de tobillos, piernas y/o abdomen

Nota: Estos síntomas también pueden surgir a raíz de otras condiciones de salud menos graves. Cualquiera que experimente estos síntomas debe consultar a un médico.


***Cáncer de los testículos.***

Los síntomas incluyen:

* Un bulto indoloro o hinchazón en cualquiera de los testículos
* Agrandamiento o hinchazón de un testículo o cambio en cómo se siente
* Sensación de peso en el escroto
* Un dolor sordo en el abdomen o ingle
* Fluido en el escroto, que surge repentinamente
* Dolor o incomodidad en un testículo o en el escroto
* Dolor en la baja espalda (en las fases más avanzadas del cáncer)
* Aumento del tamaño de los senos

Nota: Estos síntomas pueden ser ocasionados por otras condiciones de salud menos graves. El hombre que tenga estos síntomas debe consultar a un médico.


***Cáncer Diplasma Cervical.***

Síntomas:
Generalmente, no hay síntomas evidentes de la displasia cervical benigna; se detectan los cambios celulares a través de pruebas de diagnóstico.


***Cáncer de la Vejiga.***

Los síntomas incluyen:

* Sangre en la orina (generalmente indoloro)
* Disuria (dolor al orinar)
* Necesidad frecuente de orinar, generalmente cantidades pequeñas
* Dolor pélvico (raro)

Estos síntomas también pueden surgir a raíz de otras condiciones de salud menos graves. Cualquiera que experimente estos síntomas debe consultar un médico


***Cáncer Laringe.***

* Tos persistente, afonía o dolor de garganta
* Bulto anormal en la garganta o cuello
* Dificultad para tragar
* Dolor al tragar
* Atragantarse con frecuencia con comida
* Dificultad para respirar
* Respiración ruidosa
* Dolor de oído persistente
* Pérdida de peso significativa, no planeada
* Mal aliento persistente

Nota: Estos síntomas también pueden surgir a raíz de otras condiciones de salud menos graves. La persona que sienta dichos síntomas debe consultar a un médico.


***Cáncer del Colon.***

Los síntomas incluyen:

* Cambio en los hábitos intestinales
* Diarrea, constipación o sensación de que los intestinos no se vacían completamente
* Sangre (ya sea de color rojo vivo o muy oscura) en la materia fecal
* Materia fecal más delgada que lo normal
* Incomodidad abdominal (dolores frecuentes debidos a gases, hinchazón, sentirse lleno(a) y/o calambres)
* Pérdida de peso sin explicación
* Fatiga constante
* Vómitos

Nota: Estos síntomas también pueden ser ocasionados por otros problemas de salud menos graves. La persona que sienta dichos síntomas debe consultar a un médico

Fuente: foros.univision.com


***Síntomas del cáncer***

Las principales manifestaciones del cáncer se relacionan con la localización y tamaño del tumor, y con el grado de afectación de los órganos que pueden dar lugar a síntomas. A continuación se exponen los síntomas del cáncer más frecuentes, recogidos de manera sistematizada y clasificados de acuerdo a los órganos afectados:

* Aspectos generales: afectación del estado general, fiebre, astenia, anorexia, pérdida de peso.
* Digestivo: pérdida de apetito, disfagia, vómitos, alteración del ritmo intestinal, hemorragias, hipo.
* Respiratorio: tos, hemoptisis, disnea, dolor torácico.
* Cardiovascular: disnea, edemas, dolor precordial, palpitaciones.
* Genitourinario: hematuria, síntomas urinarios.
* Locomotor: dolor óseo.
* Sistema nervioso: cefalea, déficit neurológico, crisis convulsivas, alteraciones de la conciencia…
* Sistema cutáneo: alteraciones de la coloración (palidez, ictericia), picor, presencia de lesiones, tumoraciones, adenopatías…

Fuente: webconsultas.com


***Cáncer de ovario.***

El cáncer de ovario está en el segundo lugar en la ginecología. Además es el que más tasa de mortalidad tiene en este ámbito, eso se debe a que generalmente se descubre cuando la enfermedad ya está avanzada.

Aunque siempre es recomendable visitar al médico y hacer los estudios necesarios pertinentes, existen algunos indicios que pueden indicarnos que alguien padece el cáncer de ovario:

*	Presión en la pelvis 
*	Deseos de orinar muy frecuentes y con urgencias 
*	Hinchazón moderada 
*	Calambres frecuentes y molestias en el abdomen 
*	Cambios irregulares en el funcionamiento del intestino 
*	Dolor abdominal  frecuente
*	Náuseas  o vómitos
*	Perdida de los deseos de comer  
*	Aumento o reducción de peso sin causa 
*	Dolor durante las relaciones
*	Fatiga irregular

Un dato muy importante a tener en consideración, es que según datos estadísticos las mujeres que nunca han concebido un hijo, tienen un riesgo más elevado de contraer cáncer de ovario.

Fuente: www.conectanoti.com


***Cáncer de ano***

Hay un 20% de pacientes que no tiene ningún síntoma.

Hay otras enfermedades benignas que tienen los mismos síntomas, por ejemplo las fisuras anales, las fístulas o las hemorroides, por lo que hay que pensar en descartar un cáncer de ano o canal anal cuando los síntomas son persistentes y se agravan con el paso del tiempo. 

Los condilomas anales o anorectales están muy relacionados con la infección por el virus del papiloma humano (HPV), la práctica del sexo anal y la homosexualidad. Hoy día se sabe que suponen un factor de riesgo para el cáncer de ano. Así, el cáncer de ano se diagnostica en el 50% de los homosexuales con condilomas y en el 30% de las mujeres con condilomas anales y pareja heterosexual.

Cuando el tumor está más avanzado, puede originar bultos en las ingles o aparición de síntomas más generales como la fiebre, el cansancio, la pérdida de apetito y la pérdida de peso.

Los síntomas más frecuentes en el cáncer de ano son:

*	El sangrado anal. Ocurre en el 45% de los pacientes.
*	La presencia de un bulto alrededor del ano. Entre un 20 y un 30% de los casos.
*	Sensación de picor o dolor alrededor del ano. Entre un 20 y un 30% de los casos.


***Cáncer de esófago***

En fases precoces de la enfermedad, el cáncer de esófago, no produce ningún tipo de síntomas, por lo que el diagnóstico, en este momento, es generalmente accidental, es decir se diagnostica por pruebas que se realizan para descubrir otros problemas de salud.
 
En la mayor parte de las ocasiones, el cáncer de esófago se diagnostica cuando los síntomas obligan al paciente a acudir al médico.
Los síntomas más frecuentes son los siguientes: 

*	Disfagia: es el síntoma más común del cáncer de esófago. El paciente nota una dificultad para tragar y la sensación de que un alimento se ha quedado detenido en la garganta o en mitad del tórax. Esta dificultad aparece primero para alimentos sólidos, fundamentalmente el pan y la carne, por lo que el paciente inconscientemente evita tomar dichos alimentos. Progresivamente la disfagia aparece con todos los sólidos, por lo que se produce un cambio total de los hábitos alimentarios y se pasa a una dieta líquida. Según progrese la enfermedad la disfagia será también para líquidos, incluso para la saliva. 
*	Pérdida de peso: un porcentaje importante de enfermos con cáncer de esófago pierden peso. Generalmente, debido a la imposibilidad de alimentarse adecuadamente, además de producirse una pérdida de apetito y cambios en el metabolismo. 
*	Dolor retroesternal: es un síntoma poco frecuente e inespecífico, ya que, puede aparecer en procesos benignos como el reflujo gastroesofágico.

Si aparece en un enfermo de cáncer de esófago, generalmente es un signo tardío que indica la presencia de un tumor de gran tamaño. 
Otros síntomas: cuando el tumor alcanza otras estructuras cercanas al esófago puede aparecer disfonía (ronquera), tos o hipo. 

***Cáncer de estómago***

En fases precoces de la enfermedad el cáncer gástrico no produce ningún tipo de síntomas por lo que el diagnóstico, en este momento, es generalmente accidental.
 
Se diagnostica por pruebas que se realizan para descubrir otros problemas de salud. Esta es una de las razones por las que este tumor se suele diagnosticar en fases avanzadas de su crecimiento y muy pocas veces en estadios iniciales.

En general, los síntomas que puede provocar el cáncer de estómago son inespecíficos. Los más frecuentes son los siguientes: 

*	Dolor en la parte alta del abdomen, zona llamada epigastrio (boca del estómago).
*	Sensación de plenitud tras la comida (el paciente se siente lleno incluso ingiriendo poca cantidad de alimento).
*	En ocasiones pueden aparecer náuseas y vómitos.
*	Pérdida de peso: generalmente provocada por la falta de apetito y la dificultad para comer cantidades normales de alimento.
*	Si el tumor está próximo a un esfínter (el cardias o el píloro) puede disminuir o cerrar la luz del mismo e impedir que el alimento pase al estómago o al intestino (obstrucción). En este caso puede haber disfagia (dificultad al tragar).
*	El tumor, al crecer en profundidad puede afectar a un vaso sanguíneo de la pared del estómago y provocar una hemorragia más o menos importante. Esta sangre suele salir con las heces dando lugar a heces negras o melenas.
*	La pérdida constante de sangre (oculta o no) puede dar lugar a una anemia. Esta anemia se manifiesta por una serie de síntomas como cansancio, falta de aire, palidez, taquicardia (aumento del ritmo cardiaco), etc.

Es importante que sepas que estos síntomas pueden aparecer en otras enfermedades distintas al cáncer, como la úlcera gástrica.


***Cáncer de intestino delgado*** 

Los síntomas que ocasiona este tumor, independientemente de donde se localice (duodeno, yeyuno o íleon) son muy inespecíficos, vagos y, muy importante, aparecen tardíamente.
 
Esto implica que cuando se tienen síntomas la enfermedad ya suele estar en una fase localmente avanzada o metastásica.

Los síntomas más frecuentes con:

*	Dolor abdominal, generalmente intermitente y cólico (entre el 44 y 90% de los casos). Estos síntomas son más frecuentes cuanto más distal es la lesión (íleon).
*	Pérdida de peso (24 a 44%).
*	Náuseas y vómitos (17 a 64%). Cuanto más proximal (duodeno) es el tumor, más frecuentemente aparecen estos síntomas.
*	Hemorragia digestiva (23 a 41%) que puede manifestarse solamente como una anemia crónica si la pérdida de sangre es escasa pero duradera en el tiempo.
*	Obstrucción intestinal (22 a 26%).
*	Perforación intestinal (6 a 9%).

***Cáncer de páncreas*** 

En la fase inicial de la enfermedad, el cáncer de páncreas puede no llegar a producir ningún tipo de síntomas.
 
En la mayor parte de las ocasiones, el cáncer se diagnostica cuando los síntomas obligan al paciente a acudir al médico. Dependiendo de la localización del tumor los síntomas pueden variar aunque generalmente son síntomas inespecíficos.

Los más frecuentes en este tipo de cáncer son:

*	Pérdida de peso: puede ser importante, y habitualmente es debida al déficit de absorción de nutrientes. Se suele acompañar con frecuencia de anorexia (pérdida de apetito), alteraciones en las deposiciones (diarrea) y astenia (cansancio). 
*	Dolor: generalmente se manifiesta como un dolorimiento sordo en la región superior del abdomen (epigastrio), con episodios de agudización tras la ingesta o estando acostado. Es característica su irradiación hacia la espalda como si fuera un cinturón. 
*	Obstrucción del conducto biliar: cuando el tumor está situado en la cabeza del páncreas puede llegar a impedir la eliminación de las sales biliares producidas por el hígado al intestino. La retención de dichas sales provoca el aumento de la bilirrubina. Esta sustancia es un pigmento que impregna los tejidos al aumentar sus niveles en sangre, por lo que se produce ictericia (coloración amarillenta de piel y conjuntiva ocular), pudiendo provocar un prurito (picor) intenso. Habitualmente la bilirrubina se elimina por el tubo digestivo dando color a las heces. Cuando aparece una obstrucción del conducto biliar la bilirrubina se elimina por el riñón, por lo que la orina puede volverse oscura y las  heces pierden color (heces claras).
*   Náuseas y vómitos: se producen como consecuencia de la obstrucción intestinal provocada por el crecimiento de los tumores localizados en la cabeza pancreática.

Otros síntomas que pueden acompañar a este tumor son digestiones pesadas e hinchazón abdominal por acúmulo de líquido en el abdomen (ascitis).

Según los casos y la localización del tumor, en ocasiones el cáncer puede manifestarse como:

*	Pancreatitis aguda: al obstruirse los conductos de secreción de los lobulillos, puede provocar una inflamación aguda del páncreas.
*	Diabetes mellitus: gran parte de los pacientes con cáncer de páncreas, presentan una alteración del metabolismo de la glucosa, que en ocasiones acontece meses antes del diagnóstico del tumor.


***Cáncer de próstata*** 

El cáncer de próstata, como una gran parte de los tumores malignos, no va a causar ninguna alteración perceptible por el paciente en las fases iniciales de la enfermedad.
 
Son tumores que evolucionan lentamente y normalmente la sintomatología va a presentarse en etapas más avanzadas del proceso.

Los síntomas locales que pueden presentarse en el cáncer de próstata son los siguientes:  

*	Urgencia miccional: imperiosidad miccional o incluso pérdida involuntaria de la orina.
*	Aumento de la frecuencia de orinar, tanto por el día como por la noche (nicturia).
*	Disuria: dolor y escozor durante la micción (micción dolorosa).
*	Retardo en el inicio de la micción y disminución de la fuerza del chorro miccional o intermitencia del mismo.
*	En ocasiones la uretra se obstruye por el tumor y aparece retención urinaria (obstrucción).
*	Goteo posmiccional.
*	Sensación de vaciamiento incompleto de la vejiga. 
*	Crecimiento o dolor mamario.
*	Aunque poco frecuente, los tumores de células germinales pueden causar aumento de las mamas (ginecomastia)  o dolor en las mamas. Esto es debido a que este tipo de tumor puede segregar altas cantidades de una hormona llamada HCG (Gonadotropina Coriónica Humana) que favorece el crecimiento de las mamas. Un subtipo de tumor denominado tumor de células de Leydig puede producir estrógenos (hormonas sexuales femeninas), lo que puede causar crecimiento de las mamas y pérdida de deseo sexual.  Este último, en niños pueden causar signos de pubertad a una edad anormalmente temprana, como voz más profunda y crecimiento del vello facial y corporal.

***Cáncer de recto***

Los síntomas más frecuentes del cáncer de recto son:
 
*	El sangrado o rectorragia, que puede presentarse a la vez que la deposición (hematoquecia) o independientemente de ésta (rectorragia franca). Es bastante frecuente que una hematoquecia sea leve y pase desapercibida por el paciente. Ocurre en el 40% de los pacientes.
*	Estreñimiento y heces acintadas. Se debe a la obstrucción parcial del paso del contenido fecal por el recto. Ocurre entre un 40-45% de los casos.
*	Dolor abdominal o con la defecación. Se da entre un 40-45% de los casos.
*	Síntomas generales como la pérdida de peso, debilidad o anemia sin causa conocida se ven en más del 20% de los pacientes.

***Cáncer de tiroides***

El síntoma más frecuente de cáncer de tiroides es un nódulo palpable en la cara anterior del cuello. Afortunadamente, la mayoría de las veces, los nódulos son benignos; sólo 1 de cada 20 nódulos resulta ser maligno.

En otras ocasiones pueden aparecer otros síntomas menos frecuentes: 

*	Dolor en la zona anterior del cuello.
*	Dificultad para tragar si el nódulo es de gran tamaño.

***Cáncer de vagina***

A diferencia de otros tumores, el cáncer de vagina puede comenzar a dar síntomas cuando aún está en fase inicial. 
 
Los síntomas más comunes del cáncer de vagina:

*	Hemorragia vaginal anormal no relacionada con la menstruación.
*	Dolor pélvico durante el coito.
*	Presencia de un bulto en la vagina. 
*	Dificultad para orinar.

***Cáncer de vesícula biliar***

Un tumor en la vesícula biliar puede ocasionar dolor en la zona del abdomen superior derecha, también puede producir náuseas, vómitos y digestiones pesadas.

Si el tumor obstruye el paso de la bilis hacia el intestino se producirá ictericia, que significa la presencia de color amarillento en la piel y en las mucosas.

Es posible que el acúmulo de la bilis favorezca que ocurra una infección en la misma, apareciendo fiebre alta y dolor abdominal más intenso que puede ser de tipo cólico.

Cuando el tumor va creciendo de tamaño y extendiéndose por la cavidad abdominal puede dar lugar a sensación de hinchazón abdominal o incluso a acúmulo de líquido en la cavidad abdominal (a esto se le llama ascitis).

***Cáncer de vías biliares*** 

Estos tumores dan síntomas por obstrucción del drenaje de la bilis, causando ictericia (coloración amarillenta de la piel y mucosas).
 
La retención de bilis y su paso a la sangre también puede dar lugar a prurito (picor generalizado por todo el cuerpo). 

Otros síntomas frecuentes, cuando la enfermedad está más avanzada, pueden ser: dolor abdominal, pérdida de peso y anorexia… o bien pueden estar relacionados con la localización de las metástasis.

***Cáncer de piel: Melanoma***

Los melanomas por lo general no son dolorosos. La primera señal del melanoma con frecuencia es un cambio en el tamaño, forma, color, o sensación de un lunar existente.
 
Los melanomas también pueden aparecer como un lunar nuevo, negro, o anormal. Los síntomas son el resultado del crecimiento incontrolable de células cancerosas. Es importante recordar que la mayoría de personas tienen lunares, y casi todos los lunares son benignos.

En los varones suele aparecer en el tronco o la región de la cabeza o el cuello, mientras que en las mujeres suele aparecer en brazos o piernas.

Es necesario consultar al dermatólogo cuando se observen cambios en un lunar. La regla del ABCD nos puede ayudar a distinguir un lunar normal de un melanoma:

*	A: Asimetría: que la mitad de un lunar no es igual que la otra mitad.
*	B: Bordes irregulares: bordes desiguales. Irregulares, borrosos o dentados. 
*	C: Color: los colores más peligrosos son los rojizos, blanquecinos y azulados sobre lesiones de color negro.
*	D: Diámetro: cuando el lunar mide más de 6 milímetros o aumente de tamaño (mayor de 6 mm.)

***Cáncer de piel: No melanoma***

Los carcinomas cutáneos no melanoma aparecen en las zonas más frecuentemente expuestas a la luz del sol.
 
La zona de la cabeza y del cuello (55% de los casos), el dorso de las manos y los antebrazos (18% de los casos) y las piernas (13% de los casos) son los lugares más habituales dónde aparecen los carcinomas cutáneos no melanoma. En carcinoma de células basales asienta casi en el 70% de los casos en la cara y cuello. 

La lesión que el sol induce en la piel pasa por una serie de situaciones clínicas progresivamente más agresivas hasta que se desencadena un carcinoma cutáneo invasivo. La primera lesión, todavía benigna, es la llamada “queratosis actínica”.  Es una lesión levemente elevada, como una placa eritematosa con tendencia a la descamación. Entre el 1 y el 8% de estas lesiones evolucionarán a un carcinoma epidermoide de la piel. El 80% de los carcinomas epidermoides de la piel asientan sobre, o en la proximidad de queratosis actínicas. 
 
El siguiente paso en la malignización es el carcinoma epidermoide "in situ". El aspecto es de una placa, nódulo o pápula eritematosa, dura, hiperqueratósica, fija e infiltrada. Puede aparecer de forma aislada o como lesiones múltiples. 
 
Por último el carcinoma epidermoide invasivo tiene también el aspecto de un nódulo o placa dura eritematosa, de un tamaño pequeño, entre 0,5 a 1,5 cm y que puede ulcerarse y sangrar levemente. En general es una lesión asintomática, o como mucho que puede causar cierto picor.

***Carcinoma de nasofaringe  o cavum***

Debido a la localización del tumor, los síntomas aparecen cuando la lesión crece y obstruye las trompas de Eustaquio o las fosas nasales, por lo que la mayoría de los pacientes son diagnosticados con tumores grandes.

*	La mayoría de los pacientes acuden al médico por la aparición de ganglios palpables en el cuello (80% de los enfermos en el momento del diagnóstico) que se corresponde con una metástasis del tumor.
*	Cuando el tumor tiene cierto tamaño puede producir una otitis secretora (acumulo de líquido en un oído por falta de ventilación), por lo que el paciente percibe una disminución en la audición de uno o de los dos oídos.
*	Dolor de oídos (otalgia).
*	Alteración de la voz (voz nasal).
*	Otros síntomas son: epistaxis (sangrado nasal), obstrucción nasal, cefalea (dolor de cabeza).

***Leucemia linfática crónica***

El diagnóstico tiene lugar con frecuencia de forma casual, al encontrar un aumento de los linfocitos (linfocitosis) durante la realización de un análisis de sangre.

 Muchas personas están asintomáticas en el momento del diagnóstico (aproximadamente 8 de cada 10) y la sospecha se basa en las alteraciones detectadas en un análisis de rutina.

Es frecuente observar que las personas que la padecen se mantienen asintomáticas durante un tiempo variable (meses o años), precisando sólo observación y control clínico periódico. Otras presentan en el momento del diagnóstico (y muchas cuando la enfermedad avanza) cansancio, aumento del tamaño de los ganglios linfáticos, infecciones de repetición, reactivación de infecciones previas (herpes zóster), etc. Las infecciones repetidas, especialmente las respiratorias son frecuentes con el paso del tiempo y en muchas ocasiones obligan al ingreso en el hospital. El médico puede encontrar al explorar al enfermo un aumento del tamaño de los ganglios linfáticos (adenopatías), del hígado (hepatomegalia) o del bazo (esplenomegalia). Pueden encontrarse otras muchas alteraciones, como por ej.: fenómenos autoinmunes (rechazo de algunas células o tejidos propios como si fuesen del vecino).

***Leucemia mieloide crónica***

El diagnóstico con frecuencia se produce al encontrar de forma casual, con motivo de la realización de un análisis de sangre, un aumento de los leucocitos (leucocitosis) que afecta sobre todo a células grandes con gránulos (granulocitos).

 Muchas personas están asintomáticas en el momento del diagnóstico (6 de cada 10) y la sospecha se basa en las alteraciones detectadas en un análisis de rutina. 

Los síntomas más frecuentes son bastante inespecíficos: cansancio, dolor en cuadrante superior izquierdo del abdomen (por crecimiento o complicaciones en el bazo), sensación de saciedad precoz y pérdida de peso. En casos extremos, la presencia de una gran cantidad de leucocitos de gran tamaño en la sangre puede provocar una dificultad para la circulación de la sangre que motiva, alteraciones mentales y de la visión, así como dificultades respiratorias. No son raras las inflamaciones de una o varias articulaciones (artritis) por elevación del ácido úrico. Al realizar el examen físico es frecuente ( 5 de cada 10 casos) encontrar un aumento del tamaño del bazo (esplenomegalia), que se puede comprobar mejor mediante ecografía. Hace años, cuando la enfermedad tenía un peor control, podría verse una transformación brusca (crisis blástica) en la que la enfermedad tenía los síntomas de una leucemia aguda.

***Leucemias agudas***

El diagnóstico puede tener lugar de forma casual, durante la realización de un análisis de sangre o como consecuencia de un fallo brusco en el funcionamiento de la médula ósea que provoca alteraciones en los glóbulos (palidez, mareos, hemorragias, infecciones).

A veces son los dolores óseos, o el aumento de tamaño de diversos órganos, el primer signo de la enfermedad.

La alteración del funcionamiento de la médula ósea (“fallo medular”) provoca la aparición de hemorragias (por la falta de plaquetas o alteraciones de la coagulación de la sangre), infecciones (por la disminución de la capacidad defensiva del cuerpo al disminuir los glóbulos blancos normales) o manifestaciones de anemia (cansancio, palidez, mareos, zumbidos de oídos, palpitaciones, etc.)

Todos estos síntomas se dan también en otras enfermedades y es la combinación de ellos de un modo preciso y las alteraciones en los análisis de sangre, lo que sugiere al médico que la persona padece un fallo medular. En algunos tipos de leucemia puede existir un aumento de tamaño de un testículo, dolores de cabeza muy persistentes, dolores cerca de las articulaciones y otros datos de invasión extramedular.

***Linfoma no Hodgkin***

Inicialmente puede no producir síntomas, hasta que aparece un bulto grande no doloroso en el cuello, las axilas o las ingles.
 
Otras veces pueden hincharse algunos órganos como el bazo, el hígado o el estómago.

Otras veces pueden aparecer síntomas generales como por ejemplo fiebre, sudores nocturnos muy intensos, cansancio, pérdida involuntaria de peso o erupción de manchas en la piel.

***Linfoma tipo Hodgkin*** 

Lo más frecuente es que se detecte el crecimiento de algún ganglio linfático periférico, como los latero-cervicales, supraclaviculares, axilares o inguinales. Este aumento de tamaño suele ser indoloro.

 Menos veces se observa aumento del tamaño del hígado o del bazo o afectación de otros órganos.

A veces también aparecen síntomas generales como por ejemplo fiebre, sudores nocturnos intensos, cansancio, pérdida involuntaria de peso o prurito (picor en la piel). Estos síntomas generales se observan en un tercio de los pacientes.

***Mesotelioma***

En muchas ocasiones la presencia de un mesotelioma en tórax o en el abdomen, hace que se acumule líquido originando respectivamente derrame pleural o ascitis.
 
Aproximadamente dos tercios de los pacientes que se diagnostican se encuentran en el rango de edad comprendido entre los 40 y los 70 años. La disnea (o sensación de falta de aire) y el dolor torácico son los dos síntomas iniciales más frecuentes que llevan a la sospecha de la enfermedad, los cuales se presentan de manera insidiosa, con varios meses de evolución hasta ser valorados. El dolor puede referirse al hombro y abdomen superior como consecuencia de la afectación del diafragma.

A medida que la enfermedad progresa aparece el síndrome general, con disminución de peso, anorexia, tos y febrícula. La exploración física pone de manifiesto en ocasiones la pérdida de volumen del pulmón afecto así como signos de derrame pleural. Algunos pacientes se encuentran asintomáticos en el momento del diagnóstico y es infrecuente la presentación inicial como enfermedad con metástasis.

***Mieloma múltiple***

El diagnóstico puede tener lugar de forma casual, al encontrarse una anemia durante la realización de un análisis de sangre o un componente monoclonal en los análisis de sangre o de orina. Otras personas se diagnostican por fracturas “patológicas” (sin golpes ni caídas, al hacer un mínimo movimiento) o alteraciones de la función del riñón.

En ocasiones, las personas que la padecen se mantienen asintomáticas durante varios meses o años y precisan sólo control clínico periódico. Otras presentan en el momento del diagnóstico (y muchas cuando la enfermedad avanza) cansancio, dolores óseos, infecciones de repetición, etc. No es frecuente que el médico encuentre al explorar al enfermo un aumento del tamaño de los ganglios linfáticos, del hígado u otros órganos, aunque a veces se detectan deformidades en los huesos de reciente aparición.

***Osteosarcoma***

El osteosarcoma (o cáncer de huesos)  puede producirse en cualquier hueso del cuerpo humano pero es mucho más frecuente en las partes óseas cercanas a la articulación de la rodilla, como son el extremo distal del fémur y el proximal de la tibia.
 
La segunda localización más frecuente es el húmero, cerca de la articulación del hombro.

El tumor, al crecer, invade los tejidos que rodean al hueso debilitándolo. Por ello los síntomas más frecuentes son el dolor y la aparición de un engrosamiento o aumento de tamaño de una parte del hueso. El dolor se acentúa con el movimiento y la tumefacción o masa puede dificultar el movimiento de la articulación más cercana, más frecuentemente la rodilla. Como el hueso se debilita es más probable que se fracture ante traumatismos pequeños.

***Sarcoma de partes blandas***

Dada la gran heterogeneidad de este tipo de tumores, la sintomatología va a depender de la localización del tejido donde se originan, por ejemplo en un miembro es distinto a si aparecen en el abdomen.

Puede aparecer un bulto de crecimiento progresivo en alguna parte del cuerpo, que en ocasiones puede ocasionar dolor. Como es lógico, muchos de los bultos que aparecen en nuestro organismo no son malignos, como por ejemplo un lipoma, o un quiste cutáneo, o una inflamación, etc.

***Tumores del Sistema Nervioso Central***

Los síntomas de los tumores cerebrales dependen del tamaño del mismo y de su localización. En general, a estos síntomas se añaden otros secundarios al aumento de la presión intracraneal.
 
***Existen varios motivos por los que un tumor cerebral provoca un incremento de la presión intracraneal:***

*	El cerebro se encuentra en el interior de una estructura rígida formada por hueso. Al crecer el tumor, se produce un aumento del volumen del cerebro, por lo que se comprime contra las paredes del cráneo provocando una serie de síntomas.
*	El crecimiento tumoral puede bloquear el flujo del líquido cefalorraquídeo por lo que se acumula en el cerebro.
*	Los tumores malignos cerebrales provocan un edema en los tejidos circundantes, por lo que se incrementa el volumen cerebral.

***Los síntomas del aumento de la presión intracraneal son los siguientes:*** 

*	Cefalea intensa (dolor de cabeza).
*	Vómitos (generalmente por la mañana), que pueden o no ir acompañados de náuseas.
*	Cambios de personalidad y comportamiento (irritabilidad).
*	Deterioro del nivel de conciencia.
*	Somnolencia.
*	Alteración de las funciones cardiaca y respiratoria.
 
***Dependiendo de la localización tumoral los síntomas de los tumores en el cerebro varían.***

 ***Lóbulo frontal:***  

*	Parálisis o disminución de fuerza en la mitad del cuerpo (hemiplejia izda. o dcha.).
*	Mareo.
*	Alteraciones en el lenguaje.
*	Alteraciones de la memoria.
*	Alteraciones de la personalidad.
*	Pérdida de olfato.
 
***Lóbulo parietal:*** 

*	Parálisis o disminución de fuerza en la mitad del cuerpo (hemiplejia izda. o dcha.)
*	Mareos.
*	Dificultad para hablar o entender el significado de las palabras.
*	Alteración de la lectura y escritura.
*	Dificultad en la coordinación de determinados movimientos. 
*	Dificultad en la orientación del cuerpo en el espacio. 
*	Dificultad para reconocer las distintas partes del cuerpo.  
 
***Lóbulo occipital:*** 

*	Mareos.
*	Pérdida de visión de un lateral del campo visual.

***Lóbulo temporal:*** 

*	Los tumores localizados en esta zona suelen dar menos síntomas, ocasionalmente pueden provocar mareos y alteraciones en el lenguaje.
 
***Tronco del encéfalo:*** 

*	Vómitos.
*	Cefalea.
*	Marcha descoordinada.
*	Parálisis facial de un lado de la cara.
*	Dificultad para tragar (disfagia).
*	Dificultad para hablar.
*	Alteraciones en la visión.
*	Pérdida de audición. 
*	Somnolencia.
 
***Cerebelo:***    

*	Vómitos (generalmente por las mañanas y sin náuseas). 
*	Alteración en el habla.
*	Vértigos.
*	Falta de coordinación de los movimientos musculares.
	Descoordinación e inestabilidad para caminar (ataxia).

***Desde Cáncer de Ano hasta Tumores del Sistema Nervioso Central la fuente de informacion es: www.aecc.es***

***Síntomas Cáncer de boca***

Algunas formas de cáncer en la boca comienzan como una leucoplasia, es decir, lesiones blanquecinas, o lesiones enrojecidas (eritroplaquia) que no se alivian y que han estado presentes por más de 14 días. Las lesiones o úlceras pueden aparecer:

*	En la lengua, labio o cualquier otra área de la boca.
*	Usualmente de pequeño tamaño.
*	De color pálido aunque pueden ser oscuras o descoloradas.
*	Inicialmente sin dolor.
*	Con una sensación quemante o dolorosa en estados avanzados.

Otros síntomas que pueden verse asociados con esta enfermedad incluyen:

*	Problemas inusuales en la lengua.
*	Dificultad para tragar.
*	Dificultades al hablar.
*	Dolor y parestesia, característicamente como signos tardíos.

Fuente: Wikipedia.

***Síntomas Cáncer de cabeza y cuello***

Los síntomas del cáncer de cabeza y cuello varían según su localización, éstos pueden ser:

*	Labios y boca, un parche blanco o rojo en la encía, en la lengua o en el revestimiento de la boca; inflamación de la mandíbula a causa de la prótesis dental que no esté ajustada o que se siente incómoda. Así como también un sangrado o dolor poco común en la boca.
*	Faringe, dificultad para respirar o para hablar; dolor al pasar, dolor en el cuello o en la garganta que no desaparece, dolores de cabeza frecuentes, dolor o zumbido en los oídos, dificultad para oír.
*	Garganta y Laringe, los síntomas incluyen ronquera, molestia o dificultad para comer, dolor en el cuello, la mandíbula o el oído, un bulto o inflamación en el cuello o una sensación de obstrucción en la garganta.
*	Senos paranasales y cavidad nasal, senos nasales congestionados que no se despejan, sinusitis que no reacciona al tratamiento con antibióticos, sangrado por la nariz, dolores frecuentes de cabeza, inflamación u otros problemas de ojos, dolor en los dientes superiores o problemas con las prótesis dentales.
*	Glándulas salivales, hinchazón debajo del mentón o alrededor de la mandíbula, adormecimiento o parálisis de los músculos en la cara o dolor que no desaparece en la cara, mentón o cuello.

Fuente: wikipedia.

***Síntomas Cáncer de garganta***

Los principales síntomas del cáncer de garganta son:

*	Pérdida de la voz o ronquera persistente, que no mejora tras 1 o 2 semanas de tratamiento
*	Dolor en la garganta que, incluso tras la toma de medicamentos, no mejora después 1 o 2 semanas
*	Dolor y molestias en el cuello que pueden venir acompañadas de bultos en el área
*	Tos que puede venir acompañada de sangre en ocasiones
*	Sonidos extraños al respirar y dificultades para hacerlo de forma adecuada lo que puede producir fatiga y cansancio
*	En casos más avanzados dificultades para tragar y pérdida de peso

Fuente: Salud.uncomo.

***Síntomas Cáncer de garganta***

Los principales síntomas del cáncer de mama son:

*	inflamación de la mama o parte de ella
*	irritación cutánea o formación de hoyos
*	dolor de mama
*	dolor en el pezón o inversión del pezón
*	una secreción del pezón que no sea leche
*	un bulto en las axilas

Fuente: BreastCancer.org.